Name:           nnn
Version:        4.8
Release:        1%{?dist}
Summary:        nnn is a full-featured terminal file manager. It's tiny, nearly 0-config and incredibly fast

License:        https://github.com/jarun/nnn/tree/v%{version}/LICENSE
URL:            https://github.com/jarun/nnn
Source0:        %{name}-v%{version}.tar.gz

BuildRequires:  make,pkgconf-pkg-config
Requires:       readline-devel,ncurses-devel 


%description


%prep
%setup -q


%build
%set_build_flags
%make_build STRIP=/bin/true


%install
rm -rf $RPM_BUILD_ROOT
%make_install PREFIX=%{_prefix}
install -Dpm0644 -t %{buildroot}%{_datadir}/zsh/site-functions misc/auto-completion/zsh/_nnn
install -Dpm0644 -t %{buildroot}%{_datadir}/bash-completion/completions misc/auto-completion/bash/nnn-completion.bash
#make PREFIX=$RPM_BUILD_ROOT O_NOMOUSE=1 strip install


%files
%license LICENSE 
%doc CHANGELOG README.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}.1.*
%dir %{_datadir}/bash-completion
%dir %{_datadir}/bash-completion/completions
%{_datadir}/bash-completion/completions/nnn-completion.bash
%dir %{_datadir}/zsh/site-functions
%{_datadir}/zsh/site-functions/_nnn

%changelog
* Sat Apr 22 2023 Spencer Smolen <mail@spencersmolen.com>
- initial fork
